#include "displaymanager.h"
#include "ui_displaymanager.h"

DisplayManager::DisplayManager(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::DisplayManager)
{
    ui->setupUi(this);
}

DisplayManager::~DisplayManager()
{
    delete ui;
}

