#ifndef DISPLAYMANAGER_H
#define DISPLAYMANAGER_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class DisplayManager; }
QT_END_NAMESPACE

class DisplayManager : public QMainWindow
{
    Q_OBJECT

public:
    DisplayManager(QWidget *parent = nullptr);
    ~DisplayManager();

private:
    Ui::DisplayManager *ui;
};
#endif // DISPLAYMANAGER_H
